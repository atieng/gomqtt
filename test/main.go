package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"time"

	mqtt "action-target.net/gomqtt"
)

var host = flag.String("host", "localhost:1883", "hostname of broker")
var id = flag.String("id", "", "client id")
var user = flag.String("user", "", "username")
var pass = flag.String("pass", "", "password")
var dump = flag.Bool("dump", false, "dump messages?")
var delay = flag.Duration("delay", time.Second, "delay between messages")
var wait = flag.Duration("wait", 5*time.Second, "how long to pause at the end")
var count = flag.Int("count", 1000, "number of messages to send")
var who = flag.String("who", "bonnie", "who is this? (to make two instance of ticktock distinct)")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func foobar(topic string, payload string) {
	log.Printf("Got foobar message")
}

func mqconnfx(c *mqtt.Connection) {
	log.Printf("connected")
}

func mqdisco(c *mqtt.Connection) {
	log.Printf("disconnected")
}

func main() {
	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	alldone := make(chan struct{})

	m := mqtt.NewConnection(*host)
	m.OnConnect(mqconnfx)
	m.OnDisconnect(mqdisco)

	m.Subscribe("foo/bar", foobar)
	m.Subscribe("quit", func(topic string, payload string) {
		log.Printf("got shutdown message")
		close(alldone)
	})

	m.Connect()

	log.Printf("Starting publisher interval=%v", *delay)
	go func() {
		for i := 0; i < *count; i++ {
			for !m.Connected {
				time.Sleep(time.Second)
			}
			now := time.Now()
			what := fmt.Sprintf("%v at %v", *who, now)
			m.Publish("tick", []byte(what))
			time.Sleep(*delay)
		}
		if m.Connected {
			m.Publish("tick", []byte("done"))
		}
		close(alldone)

	}()

	<-alldone
}
