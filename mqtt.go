package mqtt

import (
	"bytes"
	"log"
	"net"
	"strings"
	"sync"
	"time"

	proto "github.com/huin/mqtt"
	"github.com/jeffallen/mqtt"
)

type Connection struct {
	Connected    bool
	address      string
	cc           *mqtt.ClientConn
	subs         *subscriptions
	Retry        time.Duration
	Timeout      time.Duration
	onConnect    func(*Connection)
	onDisconnect func(*Connection)
	count        int
	User         string
	Pass         string
}

func NewConnection(address string) *Connection {
	c := &Connection{
		address: address,
		Retry:   5 * time.Second,
		Timeout: 60 * time.Second,
		subs:    newSubscriptions(),
	}
	return c
}

func (c *Connection) Connect() {

	go func() {
		for {
			if c.count > 0 {
				time.Sleep(c.Retry)
				log.Printf("MQTT %s Reconnecting", c.address)
			}
			c.count++

			conn, err := net.DialTimeout("tcp", c.address, c.Timeout)
			if err != nil {
				log.Printf("%v", err)
				continue
			}

			cc := mqtt.NewClientConn(conn)
			cc.Dump = false
			cc.ClientId = ""

			if err := cc.Connect(c.User, c.Pass); err != nil {
				log.Printf("Connect %s: %v", c.address, err)
				continue
			}

			c.cc = cc
			c.Connected = true
			log.Printf("MQTT Connected to %s", c.address)

			//cc.Subscribe(c.subs.topics)
			c.cc.Subscribe(c.subs.topics)

			if c.onConnect != nil {
				c.onConnect(c)
			}

			// dispatch inbound messages
		rxmsg:
			for {
				select {
				case m, ok := <-cc.Incoming:
					if !ok { // broker disconnected
						break rxmsg
					}
					buf := new(bytes.Buffer)
					m.Payload.WritePayload(buf)
					c.subs.docallbacks(m.TopicName, buf.String())
				}
			}

			c.Connected = false
			log.Printf("MQTT %s Connection closed", c.address)
			if c.onDisconnect != nil {
				c.onDisconnect(c)
			}
		}
	}()
	return
}

func (c *Connection) OnConnect(fx func(conn *Connection)) {
	c.onConnect = fx
}

func (c *Connection) OnDisconnect(fx func(conn *Connection)) {
	c.onDisconnect = fx
}

func (c *Connection) Publish(topic string, payload []byte) {
	if c == nil || !c.Connected {
		//log.Printf("MQTT not connected.  Discard %s=%s", topic, payload)
		return
	}
	//return //DJS DEBUG
	c.cc.Publish(&proto.Publish{
		Header:    proto.Header{Retain: false},
		TopicName: topic,
		Payload:   proto.BytesPayload(payload),
	})
	//log.Printf("MQTT Publish %s=%s",topic,payload)
}

func (c *Connection) PublishString(topic string, payload string) {
	c.Publish(topic, []byte(payload))
}

type MsgCallbackFunc func(topic string, message string)
type MsgCallback struct {
	callback MsgCallbackFunc
	msg      chan string
}

type subscriptions struct {
	mu        sync.Mutex // guards access to fields below
	subs      map[string][]*MsgCallback
	wildcards []wild
	topics    []proto.TopicQos
}

func newSubscriptions() *subscriptions {
	s := &subscriptions{
		subs: make(map[string][]*MsgCallback),
	}
	return s
}

type wild struct {
	wild []string
	c    *MsgCallback
}

func newWild(topic string, c *MsgCallback) wild {
	return wild{wild: strings.Split(topic, "/"), c: c}
}

func (w wild) matches(parts []string) bool {
	i := 0
	for i < len(parts) {
		// topic is longer, no match
		if i >= len(w.wild) {
			return false
		}
		// matched up to here, and now the wildcard says "all others will match"
		if w.wild[i] == "#" {
			return true
		}
		// text does not match, and there wasn't a + to excuse it
		if parts[i] != w.wild[i] && w.wild[i] != "+" {
			return false
		}
		i++
	}

	// make finance/stock/ibm/# match finance/stock/ibm
	if i == len(w.wild)-1 && w.wild[len(w.wild)-1] == "#" {
		return true
	}

	if i == len(w.wild) {
		return true
	}
	return false
}

func isWildcard(topic string) bool {
	if strings.Contains(topic, "#") || strings.Contains(topic, "+") {
		return true
	}
	return false
}

func (w wild) valid() bool {
	for i, part := range w.wild {
		// catch things like finance#
		if isWildcard(part) && len(part) != 1 {
			return false
		}
		// # can only occur as the last part
		if part == "#" && i != len(w.wild)-1 {
			return false
		}
	}
	return true
}

func (s *subscriptions) add(topic string, c *MsgCallback) {
	s.mu.Lock()
	defer s.mu.Unlock()
	if isWildcard(topic) {
		w := newWild(topic, c)
		if w.valid() {
			s.wildcards = append(s.wildcards, w)
		}
	} else {
		s.subs[topic] = append(s.subs[topic], c)
	}

	tq := proto.TopicQos{Topic: topic, Qos: proto.QosAtMostOnce}
	s.topics = append(s.topics, tq)
	//log.Printf("MQTT topic %s added -> %v", topic, c)
}

// Find all connections that are subscribed to this topic.
func (s *subscriptions) subscribers(topic string) []*MsgCallback {
	s.mu.Lock()
	defer s.mu.Unlock()

	// non-wildcard subscribers
	res := s.subs[topic]

	// process wildcards
	parts := strings.Split(topic, "/")
	for _, w := range s.wildcards {
		if w.matches(parts) {
			res = append(res, w.c)
		}
	}

	return res
}

func (s *subscriptions) docallbacks(topic string, payload string) {

	//log.Printf("docallbacks(%s,%s)", topic, payload)
	cblist := s.subscribers(topic)
	if len(cblist) == 0 {
		log.Printf("MQTT no subscriptions for %s", topic)
		return
	}

	for _, cb := range cblist {
		//log.Printf("MQTT %s = %s callback: %v", topic, payload, cb.callback)
		if cb.callback != nil {
			//log.Printf("Do callback %v topic %s payload %s", cb.callback, topic, payload)
			(cb.callback)(topic, payload)
		}
		// add channel support there
		// cb.c <- payload
	}

}

func (c *Connection) Subscribe(topic string, callback MsgCallbackFunc) (chan string, error) {
	//log.Printf("Subscribe(%s, %v)", topic, callback)
	cb := &MsgCallback{
		callback: callback,
		msg:      make(chan string, 1),
	}
	c.subs.add(topic, cb)
	if c.Connected {
		c.cc.Subscribe(c.subs.topics[len(c.subs.topics)-1:])
	}
	return cb.msg, nil
}
